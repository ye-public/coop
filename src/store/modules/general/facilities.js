import { fetch, remove, show, store, update } from "../../../apis/facilities";

const state = {
  facilities: [],
  facility: {},
  facilitiesMetaData: {},
  facilityStatus: false,
  facilitiesDrawerStatus: false,
};

const getters = {
  facilities: (state) => state.facilities,
  facility: (state) => state.facility,
  facilitiesMetaData: (state) => state.facilitiesMetaData,
  facilityStatus: (state) => state.facilityStatus,
  facilityDrawerStatus: (state) => state.facilityDrawerStatus,
};

const mutations = {
  SET_FACILITIES(state, data) {
    state.facilities = data;
  },

  SET_FACILITY(state, data) {
    state.facility = data;
  },

  TOGGLE_FACILITY_STATUS(state, value) {
    state.facilityStatus = value;
  },

  TOGGLE_FACILITY_DRAWER(state, value) {
    state.facilityDrawerStatus = value;
  },
};

const actions = {
  FETCH_FACILITIES({ commit }) {
    commit("TOGGLE_FACILITY_STATUS", true);

    fetch()
      .then(({ data }) => {
        commit("TOGGLE_FACILITY_STATUS", false);

        commit("SET_FACILITIES", data);
      })
      .catch(() => {
        commit("TOGGLE_FACILITY_STATUS", false);
      });
  },

  STORE_FACILITY({ commit, dispatch }, form) {
    commit("TOGGLE_FACILITY_STATUS", true);

    store(form)
      .then((response) => {
        commit("TOGGLE_FACILITY_STATUS", false);
        commit("TOGGLE_FACILITY_DRAWER", false);

        if (response.status === 200) {
          dispatch("FETCH_FACILITIES");
          console.log("Done successfully"); //TODO call notification handler
        }
      })
      .catch(() => {
        commit("TOGGLE_FACILITY_STATUS", false);
      });
  },

  SHOW_SYSTEM({ commit }, id) {
    commit("TOGGLE_FACILITY_STATUS", true);

    show(id)
      .then((response) => {
        commit("TOGGLE_FACILITY_STATUS", false);

        if (response.status === 201) {
          commit("SET_FACILITY", response.data);
        }
      })
      .catch(() => {
        commit("TOGGLE_FACILITY_STATUS", false);
      });
  },

  REMOVE_FACILITY({ commit, dispatch }, id) {
    commit("TOGGLE_FACILITY_STATUS", true);

    remove(id)
      .then((response) => {
        commit("TOGGLE_FACILITY_STATUS", false);
        if (response.status === 200) {
          dispatch("FETCH_FACILITIES");
          console.log("Done successfully"); //TODO call notification handler
        }
      })
      .catch(() => {
        commit("TOGGLE_FACILITY_STATUS", false);
      });
  },

  UPDATE_FACILITY({ commit, dispatch }, data) {
    commit("TOGGLE_FACILITY_STATUS", true);

    update(data.id, data.form)
      .then((response) => {
        commit("TOGGLE_FACILITY_STATUS", false);
        if (response.status === 204) {
          dispatch("FETCH_FACILITIES");
          console.log("Done successfully"); //TODO call notification handler
        }
      })
      .catch(() => {
        commit("TOGGLE_FACILITY_STATUS", false);
      });
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
