import { fetch, remove, show, store, update } from "../../../apis/items";

const state = {
  items: [],
  item: {},
  itemsMetaData: {},
  itemStatus: false,
  itemDrawerStatus: false,
};

const getters = {
  items: (state) => state.items,
  item: (state) => state.item,
  itemsMetaData: (state) => state.itemsMetaData,
  itemStatus: (state) => state.itemStatus,
  itemDrawerStatus: (state) => state.itemDrawerStatus,
};

const mutations = {
  SET_ITEMS(state, data) {
    state.items = data;
  },

  SET_ITEM(state, data) {
    state.item = data;
  },

  TOGGLE_ITEM_STATUS(state, value) {
    state.itemStatus = value;
  },

  TOGGLE_ITEM_DRAWER(state, value) {
    state.itemDrawerStatus = value;
  },
};

const actions = {
  FETCH_ITEMS({ commit }) {
    commit("TOGGLE_ITEM_STATUS", true);

    fetch()
      .then((response) => {
        commit("TOGGLE_ITEM_STATUS", false);
        commit("SET_ITEMS", response.data);
      })
      .catch(() => {
        commit("TOGGLE_ITEM_STATUS", false);
      });
  },

  STORE_ITEM({ commit, dispatch }, form) {
    commit("TOGGLE_ITEM_STATUS", true);

    store(form)
      .then((response) => {
        commit("TOGGLE_ITEM_STATUS", false);
        commit("TOGGLE_ITEM_DRAWER", false);

        if (response.status === 200) {
          dispatch("FETCH_ITEMS");
          console.log("Done successfully"); //TODO call notification handler
        }
      })
      .catch(() => {
        commit("TOGGLE_ITEM_STATUS", false);
      });
  },

  SHOW_ITEM({ commit }, id) {
    commit("TOGGLE_ITEM_STATUS", true);

    show(id)
      .then((response) => {
        commit("TOGGLE_ITEM_STATUS", false);

        if (response.status === 201) {
          commit("SET_ITEM", response.data);
        }
      })
      .catch(() => {
        commit("TOGGLE_ITEM_STATUS", false);
      });
  },

  REMOVE_ITEM({ commit, dispatch }, id) {
    commit("TOGGLE_ITEM_STATUS", true);

    remove(id)
      .then((response) => {
        commit("TOGGLE_ITEM_STATUS", false);

        if (response.status === 204) {
          dispatch("FETCH_ITEMS");

          console.log("Done successfully"); //TODO call notification handler
        }
      })
      .catch(() => {
        commit("TOGGLE_ITEM_STATUS", false);
      });
  },

  UPDATE_ITEM({ commit, dispatch }, data) {
    commit("TOGGLE_ITEM_STATUS", true);

    update(data.id, data.form)
      .then((response) => {
        commit("TOGGLE_ITEM_STATUS", false);
        if (response.status === 200) {
          dispatch("FETCH_ITEMS");

          console.log("Done successfully"); //TODO call notification handler
        }
      })
      .catch(() => {
        commit("TOGGLE_ITEM_STATUS", false);
      });
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
