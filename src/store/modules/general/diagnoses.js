import { fetch, remove, show, store } from "../../../apis/diagnoses";

const state = {
  diagnoses: [],
  diagnosis: [],
  diagnosesMetaData: {},
  diagnosisStatus: false,
  diagnosisDrawerStatus: false,
};

const getters = {
  diagnoses: (state) => state.diagnoses,
  diagnosis: (state) => state.diagnosis,
  diagnosesMetaData: (state) => state.diagnosesMetaData,
  diagnosisStatus: (state) => state.diagnosisStatus,
  diagnosisDrawerStatus: (state) => state.diagnosisDrawerStatus,
};

const mutations = {
  SET_DIAGNOSES(state, data) {
    state.diagnoses = data;
  },

  SET_DIAGNOSIS(state, data) {
    state.diagnosis = data;
  },

  TOGGLE_DIAGNOSIS_STATUS(state, value) {
    state.diagnosisStatus = value;
  },

  TOGGLE_DIAGNOSIS_DRAWER(state, value) {
    state.diagnosisDrawerStatus = value;
  },
};

const actions = {
  FETCH_DIAGNOSES({ commit }) {
    commit("TOGGLE_DIAGNOSIS_STATUS", true);

    fetch()
      .then((response) => {
        commit("TOGGLE_DIAGNOSIS_STATUS", false);

        if (response.status === 200) {
          commit("SET_DIAGNOSES", response.data);
        }
      })
      .catch(() => {
        commit("TOGGLE_DIAGNOSIS_STATUS", false);
      });
  },

  STORE_DIAGNOSIS({ commit, dispatch }, form) {
    commit("TOGGLE_DIAGNOSIS_STATUS", true);

    store(form)
      .then((response) => {
        commit("TOGGLE_DIAGNOSIS_STATUS", false);
        commit("TOGGLE_DIAGNOSIS_DRAWER", false);

        if (response.status === 200) {
          dispatch("FETCH_DIAGNOSES");
          console.log("Done successfully"); //TODO call notification handler
        }
      })
      .catch(() => {
        commit("TOGGLE_DIAGNOSIS_STATUS", false);
      });
  },

  SHOW_DIAGNOSIS({ commit }, id) {
    commit("TOGGLE_DIAGNOSIS_STATUS", true);

    show(id)
      .then((response) => {
        commit("TOGGLE_DIAGNOSIS_STATUS", false);

        if (response.status === 201) {
          commit("SET_DIAGNOSIS", response.data);
        }
      })
      .catch(() => {
        commit("TOGGLE_DIAGNOSIS_STATUS", false);
      });
  },

  REMOVE_DIAGNOSIS({ commit, dispatch }, id) {
    commit("TOGGLE_DIAGNOSIS_STATUS", true);

    remove(id)
      .then((response) => {
        commit("TOGGLE_DIAGNOSIS_STATUS", false);

        if (response.status === 204) {
          dispatch("FETCH_DIAGNOSES");
          console.log("Done successfully"); //TODO call notification handler
        }
      })
      .catch(() => {
        commit("TOGGLE_DIAGNOSIS_STATUS", false);
      });
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
