import { fetch, remove, show, store, update } from "../../../apis/services";

const state = {
  services: [],
  service: {},
  servicesMetaData: {},
  serviceStatus: false,
  servicesDrawerStatus: false,
};

const getters = {
  services: (state) => state.services,
  service: (state) => state.service,
  servicesMetaData: (state) => state.servicesMetaData,
  serviceStatus: (state) => state.serviceStatus,
  serviceDrawerStatus: (state) => state.serviceDrawerStatus,
};

const mutations = {
  SET_SERVICES(state, data) {
    state.services = data;
  },

  SET_SERVICE(state, data) {
    state.service = data;
  },

  TOGGLE_SERVICE_STATUS(state, value) {
    state.serviceStatus = value;
  },

  TOGGLE_SERVICE_DRAWER(state, value) {
    state.serviceDrawerStatus = value;
  },
};

const actions = {
  FETCH_SERVICES({ commit }) {
    commit("TOGGLE_SERVICE_STATUS", true);

    fetch()
      .then((response) => {
        commit("TOGGLE_SERVICE_STATUS", false);

        commit("SET_SERVICES", response.data);
      })
      .catch(() => {
        commit("TOGGLE_SERVICE_STATUS", false);
      });
  },

  STORE_SERVICE({ commit, dispatch }, form) {
    commit("TOGGLE_SERVICE_STATUS", true);

    store(form)
      .then((response) => {
        commit("TOGGLE_SERVICE_STATUS", false);
        commit("TOGGLE_SERVICE_DRAWER", false);

        if (response.status === 200) {
          dispatch("FETCH_SERVICES");
          console.log("Done successfully"); //TODO call notification handler
        }
      })
      .catch(() => {
        commit("TOGGLE_SERVICE_STATUS", false);
      });
  },

  SHOW_SERVICE({ commit }, id) {
    commit("TOGGLE_SERVICE_STATUS", true);

    show(id)
      .then((response) => {
        commit("TOGGLE_SERVICE_STATUS", false);

        if (response.status === 201) {
          commit("SET_SERVICE", response.data);
        }
      })
      .catch(() => {
        commit("TOGGLE_SERVICE_STATUS", false);
      });
  },

  REMOVE_SERVICE({ commit, dispatch }, id) {
    commit("TOGGLE_SERVICE_STATUS", true);

    remove(id)
      .then((response) => {
        commit("TOGGLE_SERVICE_STATUS", false);

        if (response.status === 204) {
          dispatch("FETCH_SERVICES");
          console.log("Done successfully"); //TODO call notification handler
        }
      })
      .catch(() => {
        commit("TOGGLE_SERVICE_STATUS", false);
      });
  },

  UPDATE_SERVICE({ commit, dispatch }, data) {
    commit("TOGGLE_SERVICE_STATUS", true);

    update(data.id, data.form)
      .then((response) => {
        commit("TOGGLE_SERVICE_STATUS", false);

        if (response.status === 200) {
          dispatch("FETCH_SERVICES");
          console.log("Done successfully"); //TODO call notification handler
        }
      })
      .catch(() => {
        commit("TOGGLE_SERVICE_STATUS", false);
      });
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
