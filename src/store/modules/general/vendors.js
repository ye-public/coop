import { fetch, remove, show, store, update } from "../../../apis/vendors";

const state = {
  vendors: [],
  vendor: {},
  vendorsMetaData: {},
  vendorStatus: false,
  vendorsDrawerStatus: false,
};

const getters = {
  vendors: (state) => state.vendors,
  vendor: (state) => state.vendor,
  vendorsMetaData: (state) => state.vendorsMetaData,
  vendorStatus: (state) => state.vendorStatus,
  vendorDrawerStatus: (state) => state.vendorDrawerStatus,
};

const mutations = {
  SET_VENDORS(state, data) {
    state.vendors = data;
  },

  SET_VENDOR(state, data) {
    state.vendor = data;
  },

  TOGGLE_VENDOR_STATUS(state, value) {
    state.vendorStatus = value;
  },

  TOGGLE_VENDOR_DRAWER(state, value) {
    state.vendorDrawerStatus = value;
  },
};

const actions = {
  FETCH_VENDORS({ commit }) {
    commit("TOGGLE_VENDOR_STATUS", true);

    fetch()
      .then(({ data }) => {
        commit("TOGGLE_VENDOR_STATUS", false);

        commit("SET_VENDORS", data);
      })
      .catch(() => {
        commit("TOGGLE_VENDOR_STATUS", false);
      });
  },

  STORE_VENDOR({ commit, dispatch }, form) {
    commit("TOGGLE_VENDOR_STATUS", true);

    store(form)
      .then((response) => {
        commit("TOGGLE_VENDOR_STATUS", false);
        commit("TOGGLE_VENDOR_DRAWER", false);

        if (response.status === 200) {
          dispatch("FETCH_VENDORS");
          console.log("Done successfully"); //TODO call notification handler
        }
      })
      .catch(() => {
        commit("TOGGLE_VENDOR_STATUS", false);
      });
  },

  SHOW_VENDOR({ commit }, id) {
    commit("TOGGLE_VENDOR_STATUS", true);

    show(id)
      .then((response) => {
        commit("TOGGLE_VENDOR_STATUS", false);

        if (response.status === 201) {
          commit("SET_VENDOR", response.data);
        }
      })
      .catch(() => {
        commit("TOGGLE_VENDOR_STATUS", false);
      });
  },

  REMOVE_VENDOR({ commit, dispatch }, id) {
    commit("TOGGLE_VENDOR_STATUS", true);

    remove(id)
      .then((response) => {
        commit("TOGGLE_VENDOR_STATUS", false);

        if (response.status === 204) {
          dispatch("FETCH_VENDORS");
          console.log("Done successfully"); //TODO call notification handler
        }
      })
      .catch(() => {
        commit("TOGGLE_VENDOR_STATUS", false);
      });
  },

  UPDATE_VENDOR({ commit, dispatch }, data) {
    commit("TOGGLE_VENDOR_STATUS", true);

    update(data.id, data.form)
      .then((response) => {
        commit("TOGGLE_VENDOR_STATUS", false);

        if (response.status === 200) {
          dispatch("FETCH_VENDORS");
          console.log("Done successfully"); //TODO call notification handler
        }
      })
      .catch(() => {
        commit("TOGGLE_VENDOR_STATUS", false);
      });
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
