import { fetch, remove, show, store, update } from "../../../apis/packages";

const state = {
  packages: [],
  package: {},
  packagesMetaData: {},
  packageStatus: false,
  packagesDrawerStatus: false,
};

const getters = {
  packages: (state) => state.packages,
  package: (state) => state.package,
  packagesMetaData: (state) => state.packagesMetaData,
  packageStatus: (state) => state.packageStatus,
  packageDrawerStatus: (state) => state.packageDrawerStatus,
};

const mutations = {
  SET_PACKAGES(state, data) {
    state.packages = data;
  },

  SET_PACKAGE(state, data) {
    state.package = data;
  },

  TOGGLE_PACKAGE_STATUS(state, value) {
    state.packageStatus = value;
  },

  TOGGLE_PACKAGE_DRAWER(state, value) {
    state.packageDrawerStatus = value;
  },
};

const actions = {
  FETCH_PACKAGES({ commit }) {
    commit("TOGGLE_PACKAGE_STATUS", true);

    fetch()
      .then(({ data }) => {
        commit("TOGGLE_PACKAGE_STATUS", false);

        commit("SET_PACKAGES", data);
      })
      .catch(() => {
        commit("TOGGLE_PACKAGE_STATUS", false);
      });
  },

  STORE_PACKAGE({ commit, dispatch }, form) {
    commit("TOGGLE_PACKAGE_STATUS", true);

    store(form)
      .then((response) => {
        commit("TOGGLE_PACKAGE_STATUS", false);
        commit("TOGGLE_PACKAGE_DRAWER", false);

        if (response.status === 200) {
          dispatch("FETCH_PACKAGES");
          console.log("Done successfully"); //TODO call notification handler
        }
      })
      .catch(() => {
        commit("TOGGLE_PACKAGE_STATUS", false);
      });
  },

  SHOW_PACKAGE({ commit }, id) {
    commit("TOGGLE_PACKAGES_STATUS", true);

    show(id)
      .then((response) => {
        commit("TOGGLE_PACKAGE_STATUS", false);

        if (response.status === 201) {
          commit("SET_PACKAGE", response.data);
        }
      })
      .catch(() => {
        commit("TOGGLE_PACKAGE_STATUS", false);
      });
  },

  REMOVE_PACKAGE({ commit, dispatch }, id) {
    commit("TOGGLE_PACKAGE_STATUS", true);

    remove(id)
      .then((response) => {
        commit("TOGGLE_PACKAGE_STATUS", false);
        console.log(response.status);
        if (response.status === 204) {
          dispatch("FETCH_PACKAGES");
          console.log("Done successfully"); //TODO call notification handler
        }
      })
      .catch(() => {
        commit("TOGGLE_PACKAGE_STATUS", false);
      });
  },
  UPDATE_PACKAGE({ commit, dispatch }, data) {
    commit("TOGGLE_PACKAGE_STATUS", true);

    update(data.id, data.form)
      .then((response) => {
        commit("TOGGLE_PACKAGE_STATUS", false);
        if (response.status === 200) {
          dispatch("FETCH_PACKAGES");
          console.log("Done successfully"); //TODO call notification handler
        }
      })
      .catch(() => {
        commit("TOGGLE_PACKAGE_STATUS", false);
      });
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
