import { createStore } from "vuex";
import createPersistedState from "vuex-persistedstate";
import auth from "./modules/auth/index";
import general from "./modules/general/index";
import layout from "./modules/layout/index";
import rbak from "./modules/rbak/index";

const store = createStore({
  plugins: [
    createPersistedState({
      paths: ["auth", "layout"],
    }),
  ],

  modules: {
    auth,
    layout,
    rbak,
    general,
  },
});

export default store;
