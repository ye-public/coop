import { authApiResource } from "./base";


export const store = (form) => {
  return authApiResource().post("user-contact", form);
};

export const show = (id) => {
  return authApiResource().get("user-contact/" + id);
};

export const remove = (id) => {
  return authApiResource().delete("permissions/" + id);
};
