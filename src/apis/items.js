import { baseApiResource } from "./base";

export const fetch = () => {
  return baseApiResource().get("items");
};

export const store = (form) => {
  return baseApiResource().post("items", form);
};

export const show = (id) => {
  return baseApiResource().get("items/" + id);
};

export const remove = (id) => {
  return baseApiResource().delete("items/" + id);
};

export const update = (id, form) => {
  return baseApiResource().put("items/" + id, form);
};
