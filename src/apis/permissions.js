import { authApiResource } from "./base";

export const fetch = () => {
  return authApiResource().get("permissions");
};

export const store = (form) => {
  return authApiResource().post("permissions", form);
};

export const show = (id) => {
  return authApiResource().get("permissions/" + id);
};

export const remove = (id) => {
  return authApiResource().delete("permissions/" + id);
};
