import { baseApiResource } from "./base";

export const fetch = () => {
  return baseApiResource().get("vendors");
};

export const store = (form) => {
  return baseApiResource().post("vendors", form);
};

export const show = (id) => {
  return baseApiResource().get("vendors/" + id);
};

export const remove = (id) => {
  return baseApiResource().delete("vendors/" + id);
};

export const update = (id, form) => {
  return baseApiResource().put("vendors/" + id, form);
};
