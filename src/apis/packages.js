import { baseApiResource } from "./base";

export const fetch = () => {
  return baseApiResource().get("packages");
};

export const store = (form) => {
  return baseApiResource().post("packages", form);
};

export const show = (id) => {
  return baseApiResource().get("packages/" + id);
};

export const remove = (id) => {
  return baseApiResource().delete("packages/" + id);
};

export const update = (id, form) => {
  return baseApiResource().put("packages/" + id, form);
};
