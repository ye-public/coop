import { baseApiResource } from "./base";

export const fetch = () => {
  return baseApiResource().get("services");
};

export const fetchResources = () => {
  return baseApiResource().get("resources");
};

export const store = (form) => {
  return baseApiResource().post("services", form);
};

export const show = (id) => {
  return baseApiResource().get("services/" + id);
};

export const remove = (id) => {
  return baseApiResource().delete("services/" + id);
};

export const update = (id, form) => {
  return baseApiResource().put("services/" + id, form);
};
