import { baseApiResource } from "./base";

export const fetch = () => {
  return baseApiResource().get("facilities");
};

export const store = (form) => {
  return baseApiResource().post("facilities", form);
};

export const show = (id) => {
  return baseApiResource().get("facilities/" + id);
};

export const remove = (id) => {
  return baseApiResource().delete("facilities/" + id);
};

export const update = (id, form) => {
  return baseApiResource().put("facilities/" + id, form);
};
