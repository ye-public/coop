import '@/assets/css/app.scss';

import { createApp } from 'vue';
import App from './App.vue';

import router from './router';
import store from './store';

import ElementPlus from 'element-plus'
import 'element-plus/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en';

createApp(App)
    .use(router)
    .use(store)
    .use(ElementPlus, { locale })
    .mount('#app');
